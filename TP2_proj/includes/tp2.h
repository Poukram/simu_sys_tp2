#pragma once

#include <math.h>
#include "SDL.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef TP2_H
# define TP2_H
#define START -1
#define NORTH  0
#define WEST  1
#define SOUTH  2
#define EAST  3
#define NUMDIR 4
#define SCREEN_W 600
#define SCREEN_H 600
#define SCREEN_SCALE 1
#define SCREEN_NAME "FAZH21039406 - TP2 - Random walk printer"
#define STEP_LEN 10

void window_init(void);
void window_quit(void);
typedef struct Window Window_s;
typedef struct path_node path_node_s;
typedef struct path path_s;

typedef struct Window{
	SDL_bool running;
	
	struct {
		unsigned int w;
		unsigned int h;
		const char* name;
		SDL_Window* window;
		SDL_Renderer* renderer;
	} screen;

	void(*init)(void);
	void(*quit)(void);
} Window_s;

Window_s window;

struct path_node {
	// x and y are the starting point of the segment coordinates
	int x; 
	int y;
	int dir; // North, West, South or East from previous position

	path_node_s* next;
	path_node_s* prev;
};

struct path {
	int len;
	path_node_s* head;
	path_node_s* tail;
};

typedef struct MRG32k3a_stream {
	int64_t m1;
	int64_t m2;
	int64_t x0;
	int64_t x1;
	int64_t x2;
	int64_t y0;
	int64_t y1;
	int64_t y2;
} MRG32k3a_stream_s;

MRG32k3a_stream_s* NewMRG32k3aStream(int64_t seed);
double MRG32k3a(MRG32k3a_stream_s* st);
void PNRGTest(void);
path_s* InitNewPath(void);
void InsertNode(path_s* p, path_node_s* node);
path_node_s* MakeNewNode(path_s* orig, int dir);
void PrintPath(path_s* p);
int OpposedDirection(int dir);
path_s* RandomWalk(char type, int n);
path_s* ClassicalWalk(int n, MRG32k3a_stream_s* st);
path_s* NonReversingWalk(int n, MRG32k3a_stream_s* st);
path_s* SelfAvoidingWalk(int n, MRG32k3a_stream_s* st);
void wait_for_input(void);
void TestSuite(void);

#endif // !TP2_H