#include "tp2.h"
extern Window_s window = {
	SDL_FALSE,
	{
		SCREEN_SCALE*SCREEN_W,
		SCREEN_SCALE*SCREEN_H,
		SCREEN_NAME,
		NULL,
		NULL
	},
	window_init,
	window_quit
};

int main(int argc, char *argv[]) {
	window.init();

	path_s* walk = RandomWalk('U', 1000);

	PrintPath(walk);

	wait_for_input();

	window.quit();

	return EXIT_SUCCESS;
}