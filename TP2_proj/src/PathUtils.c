#include "tp2.h"

void PrintPath(path_s* p) {
	SDL_Point* points = malloc(sizeof(SDL_Point) * p->len);
	path_node_s* curr_n = p->head;

	for (int i = 0; i < p->len; i++) {
		(points + i)->x = curr_n->x;
		(points + i)->y = curr_n->y;

		curr_n = curr_n->next;
	}

	SDL_SetRenderDrawColor(window.screen.renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(window.screen.renderer);

	SDL_SetRenderDrawColor(window.screen.renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
	if (SDL_RenderDrawLines(window.screen.renderer, points, p->len) != 0)
		printf("SDL error while rendering line -> %s\n", SDL_GetError());
	SDL_RenderPresent(window.screen.renderer);
}

path_s* InitNewPath(void) {
	path_s* new_path = malloc(sizeof(path_s));
	new_path->head = malloc(sizeof(path_node_s));
	new_path->tail = new_path->head;
	new_path->len = 1;

	new_path->head->x = SCREEN_W / 2;
	new_path->head->y = SCREEN_H / 2;
	new_path->head->dir = -1;
	new_path->head->next = NULL;
	new_path->head->prev = NULL;

	return new_path;
}

path_node_s* MakeNewNode(path_s* orig, int dir) {
	path_node_s* new_node = malloc(sizeof(path_node_s));
	int x = orig->tail->x;
	int y = orig->tail->y;

	switch (dir)
	{
	case NORTH :
		y += STEP_LEN;
		break;
	case WEST :
		x -= STEP_LEN;
		break;
	case SOUTH :
		y -= STEP_LEN;
		break;
	case EAST :
		x += STEP_LEN;
		break;
	default:
		printf("Invalid direction for path\n");
		break;
	}

	new_node->dir = dir;
	new_node->x = x;
	new_node->y = y;
	new_node->prev = NULL;
	new_node->next = NULL;

	return new_node;
}

void InsertNode(path_s* p, path_node_s* node) {
	node->prev = p->tail;
	p->tail->next = node;
	p->tail = node;
	p->len++;
}

int OpposedDirection(int dir) {
	switch (dir)
	{
	case NORTH :
		return SOUTH;
	case SOUTH :
		return NORTH;
	case EAST :
		return WEST;
	case WEST :
		return EAST;
	case START :
		return START - 1;
	default:
		printf("Invalid direction %d\n", dir);
		break;
	}
}