#include "tp2.h"

// Where type is the type of walk and n the number of steps
path_s* RandomWalk(char type, int n) {

	MRG32k3a_stream_s* st = NewMRG32k3aStream(1284);
	path_s* walk = NULL;

	switch (type)
	{
	case 'C' :
		walk = ClassicalWalk(n, st);
		break;
	case 'S' :
		walk = NonReversingWalk(n, st);
		break;
	case 'U' :
		walk = SelfAvoidingWalk(n, st);
		break;
	default:
		printf("Incorrect type of walk : %c", type);
		break;
	}

	return walk;
}

path_s* ClassicalWalk(int n, MRG32k3a_stream_s* st) {
	path_s* p = InitNewPath();

	for (int i = 0; i < n; i++) {
		int dir = ceil(MRG32k3a(st) * NUMDIR) - 1;
		InsertNode(p, MakeNewNode(p, dir));
	}

	return p;
}

path_s* NonReversingWalk(int n, MRG32k3a_stream_s* st) {
	path_s* p = InitNewPath();

	for (int i = 0; i < n; i++) {
		int dir = 0;
		while ((dir = (ceil(MRG32k3a(st) * NUMDIR) - 1)) == OpposedDirection(p->tail->dir))
			continue;
		InsertNode(p, MakeNewNode(p, dir));
	}

	return p;
}

path_s* SelfAvoidingWalk(int n, MRG32k3a_stream_s* st) {
	path_s* p = InitNewPath();

	for (int i = 0; i < n; i++) {
		int dir = 0;
		bool valid = false;
		bool unique = true;
		bool north = false;
		bool south = false;
		bool east = false;
		bool west = false;
		path_node_s* node = NULL;

		while (!valid) {
			unique = true;
			dir = ceil(MRG32k3a(st) * NUMDIR) - 1;
			switch (dir)
			{
			case NORTH :
				north = true;
				break;
			case SOUTH :
				south = true;
				break;
			case EAST :
				east = true;
				break;
			case WEST :
				west = true;
				break;
			default:
				break;
			}

			node = MakeNewNode(p, dir);
			path_node_s* curr = p->head;

			while (curr != NULL) {
				if (curr->x == node->x && curr->y == node->y) {
					unique = false;
					break;
				}
				curr = curr->next;
			}

			if (unique)
				valid = true;
			else{
				free(node);
				if (north && south && east && west) {
					node = NULL;
					break;
				}
			}
		}
		
		if (node != NULL)
			InsertNode(p, node);
		else
			break;
	}

	return p;
}