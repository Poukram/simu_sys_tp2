#include "tp2.h"

/* Rolls two dices 1000 times and computes khi value using
 * reference probabilities of the sum results of the two dices 
**/
void PNRGTest() {
	MRG32k3a_stream_s* dice1 = NewMRG32k3aStream(1234);
	MRG32k3a_stream_s* dice2 = NewMRG32k3aStream(4321);
	int k = 1000; // number of times to roll the dices
	int d1 = 0;
	int d2 = 0;
	double th_max_khi = 15.5073; // 9 possible values - 1 -> 8
	double m = k / 9.0; // 9 possible values

	double n23 = 0;
	double n4 = 0;
	double n5 = 0;
	double n6 = 0;
	double n7 = 0;
	double n8 = 0;
	double n9 = 0;
	double n10 = 0;
	double n1112 = 0;

	double np23 = (3 * k)/ 36.0;
	double np4 = k / 12.0;
	double np5 = k / 9.0;
	double np6 = (5 * k) / 36.0;
	double np7 = k / 6.0;
	double np8 = (5 *k) / 36.0;
	double np9 = k / 9.0;
	double np10 = k / 12.0;
	double np1112 = (3 * k) / 36.0;

	for (int i = 0; i < k; i++) {
		d1 = ceil(MRG32k3a(dice1) * 6);
		d2 = ceil(MRG32k3a(dice2) * 6);

		switch (d1 + d2) {
		case 2:
			n23 += 1.0;
			break;
		case 3:
			n23 += 1.0;
			break;
		case 4 :
			n4 += 1.0;
			break;
		case 5 :
			n5 += 1.0;
			break;
		case 6 :
			n6 += 1.0;
			break;
		case 7 :
			n7 += 1.0;
			break;
		case 8 :
			n8 += 1.0;
			break;
		case 9 :
			n9 += 1.0;
			break;
		case 10 :
			n10 += 1.0;
			break;
		case 11 :
			n1112 += 1.0;
			break;
		case 12 :
			n1112 += 1.0;
			break;
		default :
			printf("Something unexpected happened\n");
		}
	}

	double khi = pow(n23 - np23, 2)/np23 + pow(n4 - np4, 2)/np4 +
		pow(n5 - np5, 2)/np5 + pow(n6 - np6, 2)/np6 + pow(n7 - np7, 2)/np7 + pow(n8 - np8, 2)/np8 +
		pow(n9 - np9, 2)/np9 + pow(n10 - np10, 2)/np10 + pow(n1112 - np1112, 2)/np1112;

	printf("Theorical maximum khi to pass is : %f , actual khi is : %f\n", th_max_khi, khi);

	if (khi <= th_max_khi)
		printf("PRNG is valid\n");
	else
		printf("PRNG is not valid\n");
}