#include "tp2.h"

#define NB_STEPS 60
#define NB_TESTS 30000

long EndToEndSqDist(path_node_s* pn1, path_node_s* pn2);

void TestSuite(void) {
	long random_m[NB_STEPS];
	long nonrev_m[NB_STEPS];
	long selfav_m[NB_STEPS];
	FILE *f;

	if (fopen_s(&f, "result.txt", "w") != 0)
	{
		printf("Error opening file!\n");
	}

	fprintf(f, "Test result for different random walk length (%d steps)\n", NB_STEPS);

	for (int k = 0; k < 3; k++) {

		switch (k)
		{
		case 0:
			fprintf(f, "Random walk\n");
			break;
		case 1:
			fprintf(f, "Non reversing walk\n");
			break;
		case 2:
			fprintf(f, "Self avoiding walk\n");
			break;
		default:
			break;
		}

		for (int i = 0; i < NB_STEPS; i++) {
			random_m[i] = 0;
			nonrev_m[i] = 0;
			selfav_m[i] = 0;

			for (int j = 0; j < NB_TESTS; j++) {
				// Ensure independent random variables
				MRG32k3a_stream_s* st = NewMRG32k3aStream(((k + 1) * 10) * (i + j));
				path_s* p = NULL;

				switch (k)
				{
				case 0:
					p = ClassicalWalk(i, st);
					random_m[i] += EndToEndSqDist(p->head, p->tail);
					break;
				case 1:
					p = NonReversingWalk(i, st);
					nonrev_m[i] += EndToEndSqDist(p->head, p->tail);
					break;
				case 2:
					p = SelfAvoidingWalk(i, st);
					selfav_m[i] += EndToEndSqDist(p->head, p->tail);
					break;
				default:
					break;
				}

				free(st);
				path_node_s* curr = p->head;
				path_node_s* tmp = NULL;

				while (curr != NULL) {
					tmp = curr->next;
					free(curr);
					curr = tmp;
				}
				free(p);
			}
			switch (k)
			{
			case 0:
				random_m[i] /= NB_TESTS;
				fprintf(f, "%ld ", random_m[i]);
				break;
			case 1:
				nonrev_m[i] /= NB_TESTS;
				fprintf(f, "%ld ", nonrev_m[i]);
				break;
			case 2:
				selfav_m[i] /= NB_TESTS;
				fprintf(f, "%ld ", selfav_m[i]);
				break;
			default:
				break;
			}
		}
		fprintf(f, "\n");
	}
	fclose(f);
}

long EndToEndSqDist(path_node_s* pn1, path_node_s* pn2) {
	return pow((pn1->x - pn2->x)/STEP_LEN, 2) + pow((pn1->y - pn2->y)/STEP_LEN, 2);
}
