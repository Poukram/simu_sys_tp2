#include "tp2.h"

MRG32k3a_stream_s* NewMRG32k3aStream(int64_t seed) {
	MRG32k3a_stream_s* new_stream = malloc(sizeof(struct MRG32k3a_stream));

	new_stream->m1 = 4294967087;
	new_stream->m2 = 4294944443;
	new_stream->x0 = 1;
	new_stream->x1 = 1;
	new_stream->x2 = seed % new_stream->m1;
	new_stream->y0 = 1;
	new_stream->y1 = 1;
	new_stream->y2 = 1;

	return new_stream;
}

double MRG32k3a(MRG32k3a_stream_s* st) {
	int64_t tx2 = st->x2, ty2 = st->y2;

	st->x2 = (1403580 * st->x1 - 810728 * st->x0) % st->m1;
	st->x0 = st->x1;
	st->x1 = tx2;

	st->y2 = (527612 * st->y2 - 1370589 * st->y0) % st->m2;
	st->y0 = st->y1;
	st->y1 = ty2;

	double res = ((st->x2 - st->y2) % st->m1) / (double)st->m1;

	if (res < 0.0)
		return res + 1.0;
	return res;
}