#include "tp2.h"

void wait_for_input(void) {
	SDL_Event event;
	while (window.running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT: {
				window.running = SDL_FALSE;
			} break;
			}
		}
	}
}

void window_init(void) {
	printf("window_init()\n");
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printf("SDL error -> %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	window.running = SDL_FALSE;
	window.screen.h = SCREEN_SCALE*SCREEN_H;
	window.screen.w = SCREEN_SCALE*SCREEN_W;
	window.screen.renderer = NULL;
	window.screen.window = NULL;
	window.init = window_init;
	window.quit = window_quit;

	unsigned int w = window.screen.w;
	unsigned int h = window.screen.h;
	const char* name = window.screen.name;

	if (SDL_CreateWindowAndRenderer(w, h, 0, &(window.screen.window), &(window.screen.renderer)) != 0) {
		printf("SDL error while creating window and renderer -> %s\n", SDL_GetError());
	}

	SDL_SetWindowTitle(window.screen.window, window.screen.name);

	window.running = SDL_TRUE;
}

void window_quit(void) {
	printf("window_quit()\n");
	SDL_Quit();
	window.running = SDL_FALSE;
}