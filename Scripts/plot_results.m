rndw = [0 1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59];
nnrw = [0 1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 100 102 104 106 108 110 113 114 116];
savw = [0 1 2 4 6 9 11 13 16 18 21 24 27 29 32 35 37 40 43 46 48 51 54 56 59 62 64 67 70 72 75 77 80 82 85 87 90 92 94 97 99 101 103 105 108 110 112 114 116 118 120 122 124 125 127 129 131 132 134 136];
steps = 1:60;

plot(steps, rndw);
hold on;
plot(steps, nnrw);
hold on;
plot(steps, savw);

title('Test suite result for different random walks');
xlabel('Max number of steps');
ylabel('Mean squared end to end displacement');
legend('Random','Nonreversing','Self Avoiding');